<html>
  <head>
	<link rel="shortcut icon" href="/static/pic/zophop.png" />
    <script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.19&libraries=places&key=AIzaSyBbnLx3yln1QXj484a5r5KiKByq_ehupcY"></script>
    <script type="text/javascript" src="/static/js/infobox.js"></script>
	<link rel="stylesheet" href="/static/routeLive/css/styles.css">	 
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="height=device-height, initial-scale=1">
	 <title>zophop</title>
	<script src='/static/routeLive/js/functions.js'></script>
	<script src='/static/routeLive/js/routeExt.js'></script>
	<script>
	var imagePath = '/static/pic/';
	var streamId = '${routeId}';
	routeID = streamId;
	var city = '${city}';
	city = city.toUpperCase();
	console.log("city :"+city);
	console.log(streamId);
	function fixOrientation()
	{
		screen.orientation.lock('portrait-primary');
		//screen[prefix + (prefix === '' ? 'l' : 'L') + 'ockOrientation'](select.value);
		//screen['lockOrientation'](select.value);
	}
	fixOrientation();
	</script>
  </head>
  <body onload='getLink()'>
	  <div>
	<header id="header">
       <div class="logo" id='ttl' style="margin-left: 10px;"><a>zophop</a></div>
       <a href="https://app.adjust.com/j3cg8i" target="_blank"><button id="btn" style="float:right; margin-right: 6px;">Download App</button></a>
   </header>
		<div id='map'></div>
		<div id='Data' style='line-height: 80%;'>
			<div id='info'>
				<div class="dataDiv" style=''>
					<div style='display: table-cell;'>Route Name</div>				
				</div>
				<div class="dataDiv" style=''>
					<div style='display: table-cell;'></div>				
				</div>
			</div>
		</div>
  	 <div id='Live1' style='border-radius:3; background:white;'><div id='circle'></div><!--<img style='vertical-align:text-bottom;' src='static/pic/favicon.png'>-->&nbsp;<span style='font-size:medium;'>LIVE</span></div>
  	 <div id='notLive' style=''><br><center><span style='font-size:200%;'>Fetching Data...</span></center></div> 
  	 <script>styling()</script>
  	 <script>
		var map = null;
		var infobox = null;
		var FromStop = null;
		var ToStop = null;
		var nextStop = null;
		var poly_line = null;
		function loadMap1() {
			var mapOptions = {
				zoom: 19,
				center: new google.maps.LatLng(10.9750, 72.8258),
				panControl: false,
				zoomControl: true,
				mapTypeControl: false,
				scaleControl: false,
				streetViewControl: false,
				overviewMapControl: false,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL,
					position: google.maps.ControlPosition.LEFT_BOTTOM
				}
			};
			console.log('let see map');
			map = new google.maps.Map(document.getElementById('map'),mapOptions);
			console.log('let see map1');
			if(map==null)
			{
				console.log('let see map1 null');
			}
			console.log('let see map1 not null');
			
			setEvents();
			setMarkers();
		}
		function setMarkers()
		{
			FromStop = create_marker(19,78);
			FromStop.setIcon(getIcon("from.png"));
			FromStop.setMap(null);
			ToStop = create_marker(19,78);
			ToStop.setIcon(getIcon("destination.png"));
			ToStop.setTitle('Destination Stop');
			ToStop.setMap(null);
			poly_line = c_poly([new google.maps.LatLng(10.9750, 72.8258)]);
		}
		function c_poly(fp) {
			var flightP = new google.maps.Polyline({
				path: fp,
				color:'black',
				geodesic: true,
				strokeColor: '#2709ba',
				strokeOpacity: 1.0,
				strokeWeight: 3
			});
			if(map == null)
			{
				console.log('map is null');  
			}
			flightP.setMap(map);
			console.log("fetchAlternatives done");
			return flightP;
		}
		loadMap1();
		loadData();
		init_RouteLive();
	</script>
	</div>
  </body>
</html>

