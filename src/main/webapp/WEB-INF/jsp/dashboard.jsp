<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="shortcut icon" href="/static/pic/eta.png" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Prakash" >
	<!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.19&libraries=places&sensor=false&key=AIzaSyBbnLx3yln1QXj484a5r5KiKByq_ehupcY"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular.min.js" type="text/javascript"></script>
	<script src="https://cdn.firebase.com/js/client/2.2.7/firebase.js"></script>
	    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <!--<script src="https://cdn.firebase.com/libs/angularfire/1.1.2/angularfire.min.js"></script>-->
    <!-- Bootstrap Core JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>    
    <title>DashBoard</title>

    <!-- Bootstrap Core CSS -->
	 <!-- Latest compiled and minified CSS -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	 <!-- Optional theme -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <!-- Custom CSS -->
    <link href="/static/css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/static/etaDashboard/css/style.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div id="wrapper" data-ng-app="conductorApp">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div id="listController" data-ng-controller="listController">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"><input class="form-control" data-ng-model="name" type="text" id="search" style="display:inline-block;" placeholder="Enter Here"></a>
                <!--<a class="navbar-brand">
                    <select style="" class="btn btn-primary" id='searchProperty'>
                        <option selected="selected" value="lastCheckIn">lastCheckIn</option>
                        <option value="name">name</option>
                    </select>
                </a>-->
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li style="color:#ffffff">
                    <a class="page-scroll">
                        Today Count : {{ getTodayCount() }}/{{ conductors.length }}
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        verisonCount
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li data-ng-repeat="ver in versionCountArray | orderBy:-count">
                            <a href="#">{{ ver.version }} - {{ ver.count }}</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
					                
                <ul class="nav navbar-nav side-nav">                  
                    <li style="" data-ng-repeat="cond in conductors | filter:name | orderBy:'-lastUpdated'">
                        <a href="" id='{{cond.userId}}' ng-click="postDetailsCon(cond.userId)">
                        	<img style="height:16px; width:16px; vertical-align:middle; text-align:center;" src="/static/pic/users.png" alt="">
                        	{{ cond.uName }}
                        	<br>
                        	<label style="font-size: 70%;">{{ cond.appVersion }}</label>
                        </a>
                    </li>
                </ul>
            </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper" class="page-wrapper" style=""><!--page to be edited-->

            <div class="container-fluid" id='detailsController' data-ng-controller="detailsController">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb row">
                            <li class="active col-lg-6">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li class="fa fa-dashboard col-lg-6 pull-right" style="">
                            		<input class='datepicker ' data-ng-model="fromDate" id='fromDate' ng-change="setChangeRange()" type="date">
                                    to
                                    <input class='datepicker' data-ng-model="toDate" id='toDate' ng-change="setChangeRange()" type="date">
                            </li>
              		        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-right">
                                        <div class="">{{ details.uName }}</div>
                                        <div class="pull-right">{{ details.agency }}</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">{{ details.phone }}</span>
                                    <span class="pull-right">{{ details.city }}</span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6 text-left">
                                        <div class=""> Trips</div>
                                        <div> distance </div>
                                        <div> time </div>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <div class=""> {{ details.totalCheckIns }}</div>
                                        <div> {{ details.totalDistanceString }} </div>
                                        <div> {{ details.totalTimeString }} </div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">Total</span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!--<div class="col-lg-4 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6 text-left">
                                        <div class=""> checkIns</div>
                                        <div> distance </div>
                                        <div> time </div>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <div class="">{{ details.rangeCount}}</div>
                                        <div> {{ details.rangeDistanceString }} </div>
                                        <div> {{ details.rangeTimeString }}</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">In Range</span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>-->
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Trips </h3>
                            </div>
                            <div class="panel-body" style="max-height:300px; overflow:scroll;  overflow-x:hidden; ">
                                <div id="morris-donut-chart"></div>
                                <ol>
                                    <li data-ng-repeat="checkin in details.checks | orderBy:'-startTime'" ng-click="select($index)" ng-class="{'active_class':$index == selected,'unactive_class':$index != selected}">
                                        <div class="tripcss" id="{{ checkin.tripId }}" ng-click="tripSelected(checkin.tripId)">
                                            {{ checkin.date }}  <span class="pull-right">started {{ checkin.hourString }}</span>
                                            <br>
                                            duration {{ checkin.timeString }}
                                        </div>
                                        <div class="metadata">
                                            <ul>
                                                <li data-ng-repeat="dataItem in checkin.metadata">
                                                    {{ dataItem.key }} --- {{ dataItem.value }}
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Map</h3>
                            </div>
                            <div class="panel-body">
                                <div id='map' style="height:400px; width:100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script>
        var base_url = '${basePath}';
        var organisation = '${organisation}'.toLowerCase();
        var subUrl = "production";
    </script>
    <script src="/static/etaDashboard/js/functions.js"></script>
    <script type="text/javascript">
	 var map = null;
	 init();
	 getList();
    </script>

</body>

</html>
