<html>
  <head>
	<link rel="shortcut icon" href="/static/pic/zophop.png" />
    <script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.19&libraries=places&key=AIzaSyBbnLx3yln1QXj484a5r5KiKByq_ehupcY"></script>
    <script type="text/javascript" src="/static/js/infobox.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="height=device-height, initial-scale=1">
	<link rel="stylesheet" href="/static/live/css/styles.css">	 
	<script src='/static/live/js/functions.js'></script>
	<script src='/static/live/js/routeExt.js'></script>
	<title>zophop</title>
	<script>
	var imagePath = '/static/pic/';
	var streamId = '${streamId}';
	var basePath = '${basePath}';
	console.log(streamId);
	function fixOrientation()
	{
		screen.orientation.lock('portrait-primary');
		//screen[prefix + (prefix === '' ? 'l' : 'L') + 'ockOrientation'](select.value);
		//screen['lockOrientation'](select.value);
	}
	fixOrientation();
	</script>
  </head>
  <body onload='getLink()'>
	  <div>
	<header id="header">
       <div class="logo" id='ttl' style="margin-left: 10px;"><a>zophop</a></div>
       <a href="https://app.adjust.com/j3cg8i" target="_blank"><button id="btn" style="float:right; margin-right: 6px;">Download App</button></a>
   </header>
		<div id='map'></div>
		<div id='Data' style='line-height: 80%;'>
			<div id='info'>
				<br><center><span style='font-size:160%;'>Fetching Data...</span></center>
				<br>
			</div>
		</div>
  	 <div id='Live1' style='border-radius:3; background:white;'><div id='circle'></div>&nbsp;<span style='font-size:medium;'>LIVE</span></div>
  	 <script>styling()</script>
  	 <script>
		var map = null;
		var infobox = null;
		var FromStop = null;
		var ToStop = null;
		var pos_marker = null;
		var nextStop = null;
		var poly_line = null;
		function loadMap1() {
			var mapOptions = {
				zoom: 19,
				center: new google.maps.LatLng(10.9750, 72.8258),
				panControl: false,
				zoomControl: true,
				mapTypeControl: false,
				scaleControl: false,
				streetViewControl: false,
				overviewMapControl: false,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL,
					position: google.maps.ControlPosition.LEFT_BOTTOM
				}
			};
			console.log('let see map');
			map = new google.maps.Map(document.getElementById('map'),mapOptions);
			console.log('let see map1');
			if(map==null)
			{
				console.log('let see map1 null');
			}
			console.log('let see map1 not null');
			
			setEvents();
			setMarkers();
			setInfoBox_Content();
		}
		function setMarkers()
		{
			pos_marker = create_marker(29,78);
			pos_marker.setIcon(getIcon(getPath(mode)));
			FromStop = create_marker(19,78);
			FromStop.setIcon(getIcon("from.png"));
			FromStop.setMap(null);
			ToStop = create_marker(19,78);
			ToStop.setIcon(getIcon("destination.png"));
			ToStop.setTitle('Destination Stop');
			ToStop.setMap(null);
			poly_line = c_poly([new google.maps.LatLng(10.9750, 72.8258)]);
		}
		function setInfoBox_Content()
		{
			var myOptions1 = {
				content: "LiveLink"
				,alignBottom:true
				,disableAutoPan: false
				,maxWidth: 0
				,pixelOffset: new google.maps.Size(-51, -30)
				,zIndex: null
				,boxStyle: { 
					//background:"white"
					textAlign: "center"
					,opacity: 1.0
					,fontSize:'small'
					,minWidth: "80px"
					,maxWidth: "120px"
					,wordWrap: 'break-word'
					,padding: '1px'
				}
				,closeBoxMargin: ""//"10px 2px 2px 2px"
				,closeBoxURL: ""//"http://www.google.com/intl/en_us/mapfiles/close.gif"
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
			};
			infobox = new InfoBox(myOptions1);
			infobox.open(map, pos_marker);
			var str = "<div style=''>";
			str+= "<div id='infoWindow' style='background-color:white;'>";
			str+="<div id='infoName'> Route Name</div>";
			str+="<label style='font-size:73%; opacity:0.6; padding:2px;' id='last'>last seen at <span id='seen'>22:30</span></label>";			
			str+="</div>";
			str+="<div style='width:100px'>";//<div style='display:inline-block; width:20px;'></div>"
			str += "<div id='triangle-down' style='display:inline-block; border-radius:2px'></div>";
			str+="<div style='display:inline-block'></div></div>"; 
			str+="</div>";
			infobox.setContent(str);
			//infobox.close();
		}
		function c_poly(fp) {
			var flightP = new google.maps.Polyline({
				path: fp,
				color:'black',
				geodesic: true,
				strokeColor: '#000000',
				strokeOpacity: 1.0,
				strokeWeight: 3
			});
			if(map == null)
			{
				console.log('map is null');  
			}
			flightP.setMap(map);
			console.log("fetchAlternatives done");
			return flightP;
		}
		loadMap1();
		loadData();
		init_RouteLive();
	</script>
	</div>
  </body>
</html>
