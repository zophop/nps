package zophop.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class NpsController
{
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody
    String getStopDetails()  {


        return ("{status:1}");
    }

    @RequestMapping(value = "/{type}/nps", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> getNpsReponse(@PathVariable String type, @RequestParam String id) throws Exception
    {


        String reponse="{\"shouldSurvey\": true,\"screens\": {\"common\": {\"idleTimeout\":5000,\"startingQuestion\": \"APMIWoyK\",\"questions\": {\"APMIWoyK\": {\"qType\": \"RATING\",\"qTypeInfo\": {\"text\": \"How likely are you to recommend Chalo app to your friends and family?\",\"lowRangeText\":\"Not likely\",\"highRangeText\":\"Very Likely\",\"threshold\": {\"3\": {\"action\":{\"positiveCta\": {\"ctaType\": \"NEXT_QUESTION\",\"ctaAction\": \"AqKSuKps\"}}},\"7\": {\"action\": {\"positiveCta\": {\"ctaType\": \"NEXT_QUESTION\",\"ctaAction\": \"OyWqqCeR\"}}},\"8\": {\"action\": {\"positiveCta\": {\"ctaType\": \"NEXT_QUESTION\",\"ctaAction\": \"LsQcRdRK\"}}}}}},\"AqKSuKps\t\": {\"qType\": \"TEXT\",\"qTypeInfo\": {\"qText\": \"How can we improve your experience?\",\"tags\": [\"abc\", \"def\",\"ghi\"],\"action\": {\"positiveCta\": {\"ctaText\": \"SUBMIT\",\"ctaType\": \"NEXT_QUESTION\", \"ctaAction\": \"BKhmoQNo\"},\"negativeCta\": {\"ctaText\": \"DISMISS\",\"ctaType\": \"DISMISS\"}}}},\"BKhmoQNo\": {\"qType\": \"THANK_YOU\",\"qTypeInfo\": {\"qText\": \"Thanks for your feedback. We are sorry to disappoint you and will try our level best to be a much better travel partner. For this, our team might reach out to you to learn more about how we can improve so that we exceed your expectations\",\"action\": {\"positiveCta\": {\"ctaText\": \"OKAY\",\"ctaType\": \"DISMISS\"}}}},\"OyWqqCeR\t\": {\"qType\": \"TEXT\",\"qTypeInfo\": {\"qText\": \"What is the primary reason for your score?\",\"tags\": [\"abc\", \"def\",\"ghi\"],\"action\": {\"positiveCta\": {\"ctaText\": \"SUBMIT\",\"ctaType\": \"NEXT_QUESTION\", \"ctaAction\": \"NhLTkBjl\"},\"negativeCta\": {\"ctaText\": \"DISMISS\",\"ctaType\": \"DISMISS\"}}}},\"NhLTkBjl\": {\"qType\": \"THANK_YOU\",\"qTypeInfo\": {\"qText\": \"Thank you for your feedback. Our goal is to make travel safer with cashless payments and that you don’t have to ever wait in a queue at the bus counter. Your feedback plays an important role in building cashless travel”,\"action\": {\"positiveCta\": {\"ctaText\": \"OK\",\"ctaType\": \"DISMISS\"}}}},\"LsQcRdRK\t\": {\"qType\": \"TEXT\",\"qTypeInfo\": {\"qText\": \"What is the one thing we can do to make you happier?\",\"tags\": [\"abc\", \"def\",\"ghi\"],\"action\": {\"positiveCta\": {\"ctaText\": \"SUBMIT\",\"ctaType\": \"NEXT_QUESTION\", \"ctaAction\": \"jjcNWZYD\"},\"negativeCta\": {\"ctaText\": \"DISMISS\",\"ctaType\": \"DISMISS\"}}}},\"jjcNWZYD\": {\"qType\": \"THANK_YOU\",\"qTypeInfo\": {\"qText\": “Thank you for your feedback. It’s great to hear that you love traveling with Chalo’s Mobile Pass. Your feedback helps us to improve further and create a better and safer travel experience for you.\",\"action\": {\"positiveCta\": {\"ctaText\": \"OKAY\",\"ctaType\": \"DISMISS\"}}}}, }}}}";
        return ResponseEntity.status(HttpStatus.OK).body(reponse);

    }





}
